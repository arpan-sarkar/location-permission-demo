package dev.arpan.location.demo

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.RequestMultiplePermissions
import androidx.annotation.RequiresPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import java.util.*

private fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


class MainActivity : AppCompatActivity() {

    private val fusedLocationProviderClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    private val locationRequest = LocationRequest.create()
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        .setInterval(2000L)
        .setFastestInterval(1000L)

    private val settingsRequest = LocationSettingsRequest.Builder()
        .addLocationRequest(locationRequest)
        .setAlwaysShow(true)
        .build()


    private val locationSettingsChangeResult =
        registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                toast("Location settings are satisfied.")
                onLocationSettingsSatisfied()
            } else {
                toast("Location settings are not satisfied.")
                showGPSNotEnabled()
            }
        }

    private val permissionResult =
        registerForActivityResult(RequestMultiplePermissions()) { permission ->
            when {
                permission[Manifest.permission.ACCESS_FINE_LOCATION] == true -> {
                    toast("Precise Location permission granted")
                    checkDeviceLocationSettings()
                }
                permission[Manifest.permission.ACCESS_COARSE_LOCATION] == true -> {
                    toast("Only approximate permission granted")
                    showPreciseLocationPermissionNeeded()
                }
                else -> {
                    toast("Location permission denied")
                    showLocationPermissionDenied()
                }
            }
        }
    private var isPreciseLocationRequestedOnce = false

    private val tv: TextView by lazy {
        findViewById(R.id.tv)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        permissionResult.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    private fun showPreciseLocationPermissionNeeded() {
        MaterialAlertDialogBuilder(this).apply {
            setMessage("Precise location permission is not granted.")
            setCancelable(false)
            setPositiveButton("Enable") { _, _ ->
                if (!isPreciseLocationRequestedOnce) {
                    isPreciseLocationRequestedOnce = true
                    permissionResult.launch(
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                    )
                } else {
                    showChangeFromSettings()
                }
            }
            setNegativeButton("Cancel") { _, _ ->
                tv.text = "Precise location permission is not granted."
            }
        }.show()
    }

    private fun showLocationPermissionDenied() {
        MaterialAlertDialogBuilder(this).apply {
            setMessage("No location permission is not granted.")
            setCancelable(false)
            setPositiveButton("Okay") { _, _ ->
                tv.text = "No location permission is not granted."
            }
        }.show()
    }

    private fun showGPSNotEnabled() {
        MaterialAlertDialogBuilder(this).apply {
            setMessage("Device GPS in not enabled.")
            setCancelable(false)
            setPositiveButton("Enable") { _, _ ->
                checkDeviceLocationSettings()
            }
            setNegativeButton("Cancel") { _, _ ->
                tv.text = "Device GPS is not enabled"
            }
        }.show()
    }

    private fun showChangeFromSettings() {
        MaterialAlertDialogBuilder(this).apply {
            setMessage("It's looks like location permission cannot be changed from here, please grant permission from settings.")
            setCancelable(false)
            setPositiveButton("Open Settings") { _, _ ->
                startActivity(
                    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                        data = Uri.fromParts("package", packageName, null)
                    }
                )
                finish()
            }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                tv.text = "Please grant permission from settings"
            }
        }.show()
    }

    private fun checkDeviceLocationSettings() {
        LocationServices.getSettingsClient(this)
            .checkLocationSettings(settingsRequest)
            .addOnCompleteListener { task ->
                try {
                    task.getResult(ApiException::class.java)
                    onLocationSettingsSatisfied()
                } catch (e: ApiException) {
                    if (e.statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        try {
                            val resolvable = e as ResolvableApiException
                            locationSettingsChangeResult.launch(
                                IntentSenderRequest.Builder(
                                    resolvable.resolution
                                ).build()
                            )
                        } catch (ignored: IntentSender.SendIntentException) {
                        } catch (ignored: ClassCastException) {
                        }
                    } else {
                        toast("Cannot change device location settings.")
                    }
                }
            }
    }

    @SuppressLint("MissingPermission")
    private fun onLocationSettingsSatisfied() {
        tv.text = "Waiting for location..."
        lifecycleScope.launch {
            awaitLocationUpdates().collect {
                tv.text = "Lat: %f, Long:%f \n\n: %s".format(it.latitude, it.longitude, Date(it.time))
            }
        }
    }


    @RequiresPermission(anyOf = [Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION])
    suspend fun awaitLocationUpdates(): Flow<Location> = callbackFlow {

        val callback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                val location = result.locations.firstOrNull()
                if (location != null) {
                    trySend(location)
                }
            }
        }

        try {
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                callback,
                Looper.myLooper()!!
            )
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

        awaitClose {
            try {
                fusedLocationProviderClient.removeLocationUpdates(callback)
            } catch (e: SecurityException) {
            }
        }
    }

}